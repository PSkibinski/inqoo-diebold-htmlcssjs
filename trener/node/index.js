// node -v

const http = require('http')
// import http from 'http';

console.log('hello node');

const server = http.createServer((req, res) => {
  console.log(req.headers)
  res.write('<h1>Hello Server</h1>')
  res.end()
})

server.listen(8080, () => {
  console.log('ready')
})