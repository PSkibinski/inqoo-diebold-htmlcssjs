var cartItems = []
var cartTotal = 0;

function addToCart(cartItems, product_id) {
  var index = cartItems.findIndex(item => item.product_id === product_id)

  if (index == -1) {
    var product = getProductById(product_id)
    return [...cartItems, {
      product_id, product, amount: 1, subtotal: product.price * 1
    }]
  } else {
    return cartItems.map(function (item) {
      if (item.product_id !== product_id) { return item }
      var amount = item.amount + 1
      return {
        ...item, amount, subtotal: item.product.price * amount
      }
    })
  }
}
// nowaTablica = addToCart(staraTablica, 123)

function updateCartView(newItems) {
  var listEl = document.querySelector('.js-cart-items')

  newItems.forEach(function (item, index) {
    var el = listEl.querySelector(`[data-item-id="${item.product_id}"]`)
    // Add New
    if (!el) {
      var el = document.createElement('div')
      el.innerHTML = `<li data-item-id="${item.product_id}" class="list-group-item js-product d-flex justify-content-between lh-sm">
          <div>
            <h6 class="my-0 js-product-name"></h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted js-item-subtotal">${item.subtotal}</span>
          <span class="close">&times;</span>
        </li>`
      el = el.firstChild
      listEl.appendChild(el)
    }
    // Update existing
    el.querySelector('.js-product-name').innerText = item.product.name
    el.querySelector('.js-item-subtotal').innerText = item.subtotal
  })
  // Remove rest
  cartItems.forEach(function (item) {
    if (newItems.indexOf(item) == -1) {
      listEl.querySelector(`[data-item-id="${item.product_id}"]`).remove()
    }
  })
  cartItems = newItems
}


function removeFromCart(product_id) {

  return cartItems.reduce(items, function (item) {
    if (item.product_id !== product_id) { return [...items, item] }
    var amount = item.amount - 1
    if (amount > 0)
      return [...items, {
        ...item, amount, subtotal: item.product.price * amount
      }]

    return items;
  }, cartItems)

}

function getProductById(product_id) {
  return products.find(function (prod) {
    return prod.id == product_id
  })
}

