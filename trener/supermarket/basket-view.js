class BasketView {
  constructor(selector) {
    this.el = document.querySelector(selector)
  }

  /**
   * 
   * @param {CartItem} item 
   */
  addItem(item) {
    const div = document.createElement('div')
    div.innerHTML = this.renderItem(item);
    const li = div.firstElementChild
    this.el.appendChild(li)
  }

  renderItem(item) {
    return `<li data-item-id="${item.product_id}"
    class="list-group-item js-product d-flex justify-content-between lh-sm">
      <div>
        <h6 class="my-0">${item.product.name}</h6>
        <small class="text-muted">${item.product.description}</small>
      </div>
      <span class="text-muted">
      ${item.amount} &times; ${item.product.price} = ${item.subtotal}
      </span>
      <span class="close">&times;</span>
    </li>`;
  }

  removeItem(item) {
    const itemEl = this.el.querySelector(`[data-item-id="${item.product_id}"]`)
    itemEl.remove()
  }

  updateItem(item) {
    const itemEl = this.el.querySelector(`[data-item-id="${item.product_id}"]`)
    itemEl.outerHTML = this.renderItem(item)
  }

}

var basketView = new BasketView('.js-cart-items')
cart.addEventListener('ItemAdded', event => basketView.addItem(event.item))
cart.addEventListener('ItemRemoved', event => basketView.removeItem(event.item))
cart.addEventListener('ItemUpdated', event => basketView.updateItem(event.item))

var basketView2 = new BasketView('.js-cart-items-2')
cart.addEventListener('ItemAdded', event => basketView2.addItem(event.item))
cart.addEventListener('ItemRemoved', event => basketView2.removeItem(event.item))
cart.addEventListener('ItemUpdated', event => basketView2.updateItem(event.item))
// cart.view = basketView

// basketView.addItem(cart.getItems()[0])
// basketView.removeItem(cart.getItems()[0])
// basketView.updateItem(cart.getItems()[0])
