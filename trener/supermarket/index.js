var tax = 23
// Apply to PROMOTED products that don`t have their own discount
var shop_discount = 0.05;

var products = [
  {
    id: '123',
    name: "Banana Pancakes",
    price: 1410,
    description: "Best pancakes ever",
    promotion: false,
    discount: 0.1,
    image: "https://via.placeholder.com/80",
    date_added: new Date(),
    category: ['pancakes'],
  },
  {
    id: '234',
    name: "Strawbery Pancakes",
    price: 2344,
    description: "Ineresting pancakes",
    promotion: true,
    image: "https://via.placeholder.com/80",
    date_added: new Date(),
    category: ['pancakes'],
    rating: { votes: 123, rate: 4.5 }
  },
  {
    id: '345',
    name: "Banana cookies",
    price: 1243,
    description: "Awesome cookies",
    promotion: true,
    discount: 0.00,
    image: "https://via.placeholder.com/80",
    date_added: new Date(2021, 10, 1),
    category: ['cookies'],
    rating: { votes: 2343, rate: 3.5 }
  },
]

// var promotedBtn = document.getElementById('promotedBtn');
// promotedBtn.onclick = function(){
//   filter_promoted = !filter_promoted
//   renderProducts()
// }

var promotedCheckbox = document.getElementById('promotedCheckbox')
// promotedCheckbox.onclick = (function (event) {
promotedCheckbox.addEventListener('click', function (event) {
  // promotedCheckbox === event.target 
  // filter_promoted = event.target.checked 
  // filter_promoted = event.target.checked 
  filter_promoted = promotedCheckbox.checked
  renderProducts()
})


var refreshBtn = document.getElementById('refreshBtn');
// refreshBtn.onclick = renderProducts
refreshBtn.onclick = function () {
  renderProducts(products)
}

/* Filters */
var filter_promoted = true;
var limit = 2
var order = 'asc'

renderProducts()

function renderProducts() {
  var count = 0;
  var local_products = order === 'asc' ? products : products.reverse()
  productList.innerHTML = ''

  for (let product of local_products) {

    if (filter_promoted == true && product.promotion == false) continue;

    if (count++ > limit) break;

    renderToDocument(product);
  }
}

function renderToDocument(product) {

  var description = getCategoryDescription(product);
  var nett_price = getDiscountedNettPrice(product);
  var gross_price = getGrossPrice(nett_price);

  var item = `<div class="list-group-item">
    <div class="row">
      <div class="col flex-grow-0"><img src="${product.image}" alt=""></div>
      <div class="col">
        <h3>${product.name}</h3>
        <p> ${description} </p>
      </div>
      <div class="col flex-grow-0 d-grid gap-1">
        <h5>${gross_price / 100} PLN</h5>
        <button class="btn btn-info text-nowrap">Add to cart</button>
      </div>
    </div>
  </div>`
  // console.log(item)
  productList.innerHTML += item
}

function renderToConsole(productInfo) {
  var description = getCategoryDescription(product);
  var nett_price = getDiscountedNettPrice(product);
  var gross_price = getGrossPrice(nett_price);

  /* == PRODUCT INFO DISPLAY == */
  var productInfo = count + '. ' + product.name + ' ' + description + ' ' + gross_price.toFixed(2) + (product.promotion ? ' PROMOTION ' : '') + product.date_added;

  console.log(productInfo)
}

/**
 * Calculate Gross price using global tax
 * @param {number} nett_price 
 * @returns number
 */
function getGrossPrice(nett_price) {
  return Math.round(nett_price * (1 + tax / 100));
}

function getDiscountedNettPrice(product) {
  var discount = 'number' === typeof product.discount ? product.discount : shop_discount;
  var nett_price = (product.price * (1 + (product.promotion ? discount : 0)));
  return nett_price;
}

function getCategoryDescription(product) {
  var description = product.description

  switch (product.category[0]) {
    case 'pancakes':
      description += ' Amerykanskie '; /* + 'Świetne nalesniki '; break; */
    case 'nalesnik':
      description += ' Świetne nalesniki '; break;
    case 'ciastka': description += ' Pyszne ciastka '; break;
    default: description += ' Inne słodkości';
  }

  return description;
}