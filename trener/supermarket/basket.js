class AbstractCartItem { }
class CartItem extends AbstractCartItem {
  constructor(product, amount = 1) {
    this.product_id = product.id
    this.product = product
    this._updateAmount(amount)
  }

  increment() { this._updateAmount(this.amount + 1) }
  decrement() { this._updateAmount(this.amount - 1) }
  _updateAmount(amount) {
    if (amount <= 0) { throw new Error('Item amount cannot be 0') }
    this.amount = amount
    this.subtotal = this.product.price * this.amount
  }
}
class DiscountedCartItem extends AbstractCartItem {

}

class ItemAddedEvent extends Event {
  constructor(item) {
    super('ItemAdded')
    this.item = item
  }
}
class ItemRemovedEvent extends Event {
  constructor(item) {
    super('ItemRemoved')
    this.item = item
  }
}
class ItemUpdatedEvent extends Event {
  constructor(item) {
    super('ItemUpdated')
    this.item = item
  }
}

class Cart extends EventTarget {

  constructor() {
    super()

    /** @type AbstractCartItem[] */
    this._cartItems = []
    this._cartTotal = 0
  }

  reset() {
    this._cartItems = []
    this._cartTotal = 0
  }

  addProduct(product_id) {
    let item = this._cartItems.find(item => item.product_id === product_id)

    if (item) {
      item.increment()
      this.dispatchEvent(new ItemUpdatedEvent(item))
    } else {
      const product = this.products.getById(product_id)
      item = new CartItem(product)
      this._cartItems.push(item)
      this.dispatchEvent(new ItemAddedEvent(item))
    }
    this._cartTotal += item.product.price
  }

  getTotal() {
    return this._cartItems.reduce((sum, item) => sum + item.subtotal, 0)
  }

  removeProduct(product_id) {
    const item = this._cartItems.find(item => item.product_id === product_id)

    if (item) {
      try {
        item.decrement()
        this.dispatchEvent(new ItemUpdatedEvent(item))
      } catch (error) {
        this._cartItems = this._cartItems.filter(i => i.product_id !== item.product_id)
        this.dispatchEvent(new ItemRemovedEvent(item))
      }
      this._cartTotal -= item.product.price
    }
  }

  getItems() {
    return this._cartItems
  }
}

class Products {

  /**
   * Collection of Products 
   * @param {Product[]} products 
   */
  constructor(products) {
    this._products = products
  }

  /**
   * Find product By Id
   * @param {string} product_id 
   * @returns Product
   */
  getById(product_id) {
    return this._products.find(product => product.id === product_id)
  }
}

var cart = new Cart()
cart.products = new Products(window.products)

// window.basket = new Cart()

// makeCart(window.cart = {})
// makeCart(window.basket = {})


/**  @type Product */
    // var prod = productsGetById('123')
    // prod.name.toUpperCase()

/**
 * @typedef Product
 * @property {string} id
 * @property {string} name
 * @property {number} price Nett price in cents
 */

/**
 * @typedef CartItem
 * @property {string} product_id
 * @property {Product} product
 * @property {number} amount
 * @property {number} subtotal
 */
