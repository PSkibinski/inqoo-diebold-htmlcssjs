
```js

// var product3 = {
//   id:'234',
//   name:'Chocolate cookies',
//   price: 1234,
//   // description: product2.description,
//   // promotion: product2.promotion,
//   // tax: product2.tax,
//   // image: product2.image,
//   // date_added: product2.date_added,
//   // category: product2.category,
//   // rating: product2.rating,
// }
// var product3 = Object.assign({}, product2)
// product3.rating = Object.assign({}, product2.rating)
// product3.id = '234'
// product3.price = 1420
// product3.name = 'Chocolate cookies'

// var product3 = Object.assign({}, product2, {
//   rating: Object.assign({}, product2.rating),
//   id: '234',
//   price: 1420,
//   name: 'Chocolate cookies',
// })

var product3 = {
  ...product2,
  rating: { ...product2.rating },
  id: '234',
  price: 1420,
  name: 'Chocolate cookies',
}

var product4 = JSON.parse(JSON.stringify(product2))
product4.id = '234'
product4.price = 1420
product4.name = 'Chocolate cookies'

// product3.category = 'ciastka' // value
// // product3.rating.rate = 4; // reference
// product3.rating = { rate:4, votes:1 } // value

// var product3 = product2
// product3.name = 'Apple pancakes'
