const loan_amount = document.querySelector('#loan_amount');
const loan_months = document.querySelector('#loan_months');
const interest_rate = document.querySelector('#interest_rate');
const calculateBtn = document.querySelector('.js-calculate');

const calculate = (credit, instalment, interestRate) => {
    const capital = parseFloat((credit / instalment).toFixed(2));
    const tbody = document.querySelector('tbody');
    tbody.innerHTML = '';

    for (let i = 1; i <= instalment; i++) {
        const tr = document.createElement('tr');

        let td = `
            <td>${i}</td>
            <td>${capital}</td>
            <td>${parseFloat((credit / instalment * interestRate / 100).toFixed(2))}</td>
            <td>${parseFloat(credit.toFixed(2))}</td>
            <td>${parseFloat(((credit / instalment * interestRate / 100)) + capital).toFixed(2)}</td>
        `;

        credit -= capital;

        tr.innerHTML = td;
        tbody.append(tr);
    }
}

calculateBtn.addEventListener('click', () => {calculate(loan_amount.valueAsNumber, loan_months.valueAsNumber, interest_rate.valueAsNumber)});

