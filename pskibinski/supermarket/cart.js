var cartItems = [
    
]

function getProductById(productId) {
    for (let product of products) {
        if (parseInt(product.id) === productId) {
            return product;
        }
    }
}

function addProductToCart(productId) {
    var product = getProductById(productId);
    var item;

    for (let line of cartItems) {
        if (parseInt(line.productId) === productId) {
            item = line;
            break;
        }
    }

    if (!item) {
        cartItems.push({
            productId: productId.toString(),
            product,
            amount: 1,
            subtotal: product.price
        })
    } else {
        item.amount ++;
        item.subtotal += product.price;
    }

    renderCartList();
}

function renderItems() {
    var items = '';

    for (let line of cartItems) {
        items += `
            <li class="list-group-item">
                <div class="row">
                    <div class="col-7">
                        <h4>${line.product.name}</h4>
                    </div>
                    <div class="col-3 d-flex">
                        <p class="price">$${line.subtotal / 100}</p>
                    </div>
                    <div class="col-2 d-flex">
                        <button class="btn btn-danger">X</button>
                    </div>
                </div>
            </li>
        `
    }

    return items;
}

function renderTotal() {
    var total = 0;
    for (line of cartItems) {
        total += line.subtotal;
    }

    return `
        <li class="list-group-item mt-4 border-start-0 border-end-0">
            <div class="row">
                <div class="col-7">
                    <h4>Total:</h4>
                </div>
                <div class="col-5">
                    <h4 class="total">$${total / 100}</h4>
                </div>
            </div>
        </li>
    `
}

function renderCartList() {
    var cartList = document.querySelector('#cartList');
    cartList.innerHTML = '';

    cartList.innerHTML = renderItems();

    cartList.innerHTML += renderTotal(); 
}

addProductToCart(123);
addProductToCart(124);
addProductToCart(123);
