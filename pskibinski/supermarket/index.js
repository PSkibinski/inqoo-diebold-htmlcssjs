var tax = 23;
var shop_discount = 0.5;

var products = [
    {
        id: '123',
        name: 'Pancakes',
        price: 1410,
        description: 'Best pancakes ever',
        promotion: true,
        discount: 0.1,
        date_added: new Date(),
        category: 'pancakes',
        rating: {votes: 12, rate: 4.5}
    },
    {
        id: '124',
        name: 'Banan',
        price: 2500,
        description: 'Best banana ever',
        promotion: true,
        date_added: new Date(),
        category: 'fruits',
        rating: {votes: 12, rate: 4.5}
    },
    {
        id: '125',
        name: 'Cookies',
        price: 1800,
        description: 'Best cookies ever',
        promotion: true,
        date_added: new Date(),
        category: 'cookies',
        rating: {votes: 12, rate: 4.5}
    },
    {
        id: '126',
        name: 'Milk Chocolate',
        price: 1800,
        description: 'Best chocolate ever',
        promotion: false,
        date_added: new Date(),
        category: 'chocolate',
        rating: {votes: 12, rate: 4.5}
    }
]

renderProducts();

var refreshBtn = document.querySelector('#refresh');
refreshBtn.addEventListener('click', renderProducts);

function calcTotal() {
    var sum = 0;

    for (let product of cart) {
        sum += getGrossPrice(product.price);
    }

    return sum;
}

function getGrossPrice(nett_price) {
    return Math.round(nett_price * (1 + tax / 100)) / 100;
} 

function renderProducts() {
    products = products.reverse();

    var generatedHtml = '';
    for (let product of products) {
        generatedHtml += renderToDocument(product);
    }

    productsList.innerHTML = generatedHtml;
}

function getProductInfo(product) {
    var description = getCategoryDescription(product.description, product.category)
    var nett_price = product.price;
    var gross_price = getGrossPrice(nett_price);
    var productInfo = `${product.name} - ${description} - $${gross_price} - ${product.category} - ${product.promotion ? 'promo' : ''} 
    ${product.date_added.toLocaleDateString()}`;

    if (product.promotion) {
        if ('number' === typeof product.discount) {
            nett_price = product.price * (1 - product.discount);
        } else {
            nett_price = product.price * (1 - shop_discount);
        }
    }

    return productInfo;

    function getCategoryDescription(description, category) {
        switch(category) {
            case 'pancakes':
                description += ' Amerykańskie'
            case 'cookies':
                description += ' Najlepsze'
                break;
            case 'fruits':
                description += ' Smaczne'
                break;
        }
        return description;
    }

    function getGrossPrice(nett_price) {
        return Math.round(nett_price * (1 + tax / 100)) / 100;
    } 
}

// ===== Rendering ======

function renderToDocument(product) {
    var description = getCategoryDescription(product.description, product.category);
    var grossPrice = getGrossPrice(product.price);

    var item = `
        <li class="list-group-item">
            <div class="row justify-content-around">
                <div class="col-6">
                    <h4 class="js-productName">${product.name}</h4>
                    ${description}
                </div>
                <div class="col-4 d-flex align-items-center flex-column">
                    <h4 class="js-productPrice">$${grossPrice}</h4>
                    <button class="btn btn-info text-nowrap js-addToCart">Add to cart</button>
                </div>
            </div> 
        </li>
    `
    return item;

    function getCategoryDescription(description, category) {
        switch(category) {
            case 'pancakes':
                description += ' Amerykańskie'
            case 'cookies':
                description += ' Najlepsze'
                break;
            case 'fruits':
                description += ' Smaczne'
                break;
        }
        return description;
    }   
}

