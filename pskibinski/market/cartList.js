// const { products } = require('./productsList');
// const { CartItem } = require('./cartItem');

class CartList {
    constructor() {
        this.list = [];
    }

    getItemCartById(id) {
        return this.list.find((cartItem) => cartItem.product.id === id);
    }

    addItemToCart(id) {
        const cartItem = this.getItemCartById(id);
        const quantity = document.querySelector('.js-itemsQuantity');

        if (cartItem) {
            cartItem.incrementQuantity();
            quantity.innerHTML = this.getItemsQuantityInCart();
            cartView.displayView(cart.list);
        } else {
            const product = products.getProductById(id);
            product
                .then((item) => new CartItem(item))
                .then((cartItem) => this.list.push(cartItem))
                .then(() => {
                    quantity.innerHTML = this.getItemsQuantityInCart();
                    cartView.displayView(cart.list);
                });
        }
    }

    removeItemFromCart(id) {
        const cartItem = this.getItemCartById(id);
        const quantity = document.querySelector('.js-itemsQuantity');

        if (cartItem) {
            cartItem.decrementQuantity();
            cartView.displayView(cart.list);
            if (cartItem.quantity <= 0) {
                this.list = this.list.filter(
                    (cartItem) => cartItem.product.id !== id
                );
                cartView.displayView(cart.list);
            }
        } else {
            console.log('There is no such item in cart.');
        }

        quantity.innerHTML = this.getItemsQuantityInCart();
    }

    getItemsQuantityInCart() {
        return this.list.reduce((sub, cartItem) => {
            return sub + cartItem.quantity;
        }, 0);
    }

    getTotal() {
        const total = this.list.reduce((total, cartItem) => {
            return total + cartItem.subtotal;
        }, 0);

        return total / 100;
    }
}

// module.exports = { cart };
