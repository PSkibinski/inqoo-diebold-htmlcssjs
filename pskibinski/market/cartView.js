class CartView {
    constructor() {}

    displayView(itemList) {
        const cartList = document.querySelector('.js-cartList');
        cartList.innerHTML = '';
        const total = cart.getTotal();

        itemList.forEach((item) => {
            const div = document.createElement('div');
            div.innerHTML = `
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold fs-5">
                            ${item.product.name.toUpperCase()}
                        </div>
                        <button class="btn btn-primary btn-sm js-addItem">
                            &plus;
                        </button>
                        <button class="btn btn-danger btn-sm js-removeItem">
                            &minus;
                        </button>
                    </div>
                    <div class="d-flex flex-column justify-content-between">
                        $${item.subtotal / 100}
                        <span class="badge bg-primary rounded-pill align-self-end">
                            ${item.quantity}
                        </span> 
                    </div>
                </li>
            `;

            div.querySelector('.js-addItem').addEventListener('click', () => {
                cart.addItemToCart(item.product.id);
            });

            div.querySelector('.js-removeItem').addEventListener(
                'click',
                () => {
                    cart.removeItemFromCart(item.product.id);
                }
            );

            cartList.append(div.firstElementChild);
        });

        if (total) {
            const d = document.createElement('div');
            d.innerHTML = `
            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold fs-5">
                        Total: $${total}
                    </div>
                </div>
            </li
        `;
            cartList.append(d.firstElementChild);
        }
    }
}
