class ProductsSearch {
    constructor() {}

    async searchProducts(e) {
        const list = await products.getAllProducts();
        const filtered = list.filter((item) =>
            item.name.includes(e.target.value.toLowerCase())
        );
        return filtered;
    }
}
