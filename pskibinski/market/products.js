const express = require('express');
const app = express();

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.get('/products', function (req, res) {
    res.json([
        {
            id: 123,
            name: 'banana',
            desc: 'best bananas ever',
            price: 405,
            img: 'https://unsplash.com/photos/zrF6ACPLhPM/download?force=true&w=640',
        },
        {
            id: 124,
            name: 'carrot',
            desc: 'best carots ever',
            price: 200,
            img: 'https://unsplash.com/photos/wHCe4X6Kvdo/download?force=true&w=640',
        },
        {
            id: 125,
            name: 'potato',
            desc: 'best potatos ever',
            price: 150,
            img: 'https://unsplash.com/photos/B0s3Xndk6tw/download?force=true&w=640',
        },
        {
            id: 126,
            name: 'apple',
            desc: 'best apples ever',
            price: 245,
            img: 'https://unsplash.com/photos/I58f47LRQYM/download?force=true&w=640',
        },
        {
            id: 127,
            name: 'grapes',
            desc: 'best grapes ever',
            price: 245,
            img: 'https://unsplash.com/photos/wRaFm-QFjRo/download?force=true&w=640',
        },
        {
            id: 128,
            name: 'lemon',
            desc: 'best lemons ever',
            price: 245,
            img: 'https://unsplash.com/photos/ohNxxapID_k/download?force=true&w=640',
        },
        {
            id: 129,
            name: 'orange',
            desc: 'best oranges ever',
            price: 245,
            img: 'https://unsplash.com/photos/_lZiNbnZVGY/download?force=true&w=640',
        },
        {
            id: 130,
            name: 'pear',
            desc: 'best pears ever',
            price: 245,
            img: 'https://unsplash.com/photos/RW6Wz9QaoKk/download?force=true&w=640',
        },
        {
            id: 131,
            name: 'pineapple',
            desc: 'best pineapples ever',
            price: 245,
            img: 'https://unsplash.com/photos/HrdNNoG_y-A/download?force=true&w=640',
        },
        {
            id: 132,
            name: 'plum',
            desc: 'best plums ever',
            price: 245,
            img: 'https://unsplash.com/photos/Yf216RUTndU/download?force=true&w=640',
        },
        {
            id: 133,
            name: 'radish',
            desc: 'best radishes ever',
            price: 245,
            img: 'https://unsplash.com/photos/3vCaADeY8aM/download?force=true&w=640',
        },
        {
            id: 134,
            name: 'cauliflower',
            desc: 'best cauliflowers ever',
            price: 245,
            img: 'https://unsplash.com/photos/r5oZ-DJ2E8I/download?force=true&w=640',
        },
        {
            id: 135,
            name: 'corn',
            desc: 'best corn ever',
            price: 245,
            img: 'https://unsplash.com/photos/P-zN6GfHv8M/download?force=true&w=640',
        },
        {
            id: 136,
            name: 'cabbage',
            desc: 'best cabbages ever',
            price: 245,
            img: 'https://unsplash.com/photos/DzN-T3ceOMI/download?force=true&w=640',
        },
        {
            id: 137,
            name: 'red pepper',
            desc: 'best red peppers ever',
            price: 245,
            img: 'https://unsplash.com/photos/J6W_MSU67o8/download?force=true&w=640',
        },
    ]);
});

app.listen(3000, () => {
    console.log('app listening on port 3000');
});
