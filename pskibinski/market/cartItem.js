class CartItem {
    constructor(product, quantity = 1) {
        this.product = product;
        this.quantity = quantity;
        this.subtotal = this.product.price;
    }

    incrementQuantity() {
        this.quantity++;
        this.subtotal = this.product.price * this.quantity;
    }

    decrementQuantity() {
        this.quantity--;
        this.subtotal = this.product.price * this.quantity;
    }
}

// module.exports = { CartItem };
