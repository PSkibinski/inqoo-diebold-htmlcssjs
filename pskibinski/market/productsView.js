// const { products } = require('./productsList');

class ProductsView {
    displayProducts(products) {
        const productsList = document.querySelector('.js-products-list');
        productsList.innerHTML = '';

        products.then((list) =>
            list.forEach((item) => {
                const div = document.createElement('div');
                div.innerHTML = `
                        <div class="col-3 d-flex justify-content-center">
                            <div class="card" style="width: 18rem;">
                                <img src=${item.img}
                                    class="card-img-top" alt="...">
                                <div class="card-body d-flex flex-column justify-content-end align-items-start">
                                    <h5 class="card-title">
                                        ${item.name.toUpperCase()} - $${
                    item.price / 100
                }
                                    </h5>
                                    <p class="card-text">${item.desc}</p>
                                    <button class="btn btn-primary js-addToCart">
                                        Add to cart 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-plus ms-2" viewBox="0 0 16 16">
                                            <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/>
                                            <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    `;
                div.querySelector('.js-addToCart').addEventListener(
                    'click',
                    () => cart.addItemToCart(item.id)
                );
                productsList.append(div.firstElementChild);
            })
        );
    }
}

// module.exports = { productsView };
