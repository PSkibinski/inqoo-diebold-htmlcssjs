// const { list } = require('./products');

class ProductList {
    constructor(listItems) {
        this.listItems = listItems;
    }

    getProductById(id) {
        const item = this.listItems.then((itemList) =>
            itemList.find((item) => item.id === id)
        );
        if (item) return item;
    }

    getAllProducts() {
        return this.listItems;
    }
}

// module.exports = { products };
