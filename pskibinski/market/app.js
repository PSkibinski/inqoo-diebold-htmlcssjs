const products = new ProductList(
    fetch('http://localhost:3000/products')
        .then((res) => res.json())
        .then((data) => data)
);

const cartView = new CartView();
const cart = new CartList();
const productsView = new ProductsView();
productsView.displayProducts(products.getAllProducts());

const productsSearch = new ProductsSearch();
const searchBar = document.querySelector('.js-searchbar');
searchBar.addEventListener('keyup', (e) => {
    productsView.displayProducts(productsSearch.searchProducts(e));
});
